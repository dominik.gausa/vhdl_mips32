int main(void){
// get an Addresspointer
    volatile unsigned int* ptr;
    ptr = (unsigned int*)0x40000000;

// Write some Values into RAM
    int i = 0;
    for (;i<10;i++) {
	*ptr = i;
	ptr++;
    }

// do some nothingness
    asm("nop \n");

// This Pointer points to an periferal Register 
// which toggles after some time from non Zero to Zero
    volatile unsigned int* GPIOA_IN  = (unsigned int*)0x10000000;
    volatile unsigned int* GPIOA_OUT = (unsigned int*)0x10000004;
    ptr = (unsigned int*)0x40000000;

// As long as the periferal Register is != Zero 
// write our Iteration into RAM Addr 240
    for (i=0;;i++) {
        if(*GPIOA_IN == 0)
	    break;
	*GPIOA_OUT = 42;
	*ptr = i;
	}

    while(1)
	asm("nop \n");
}
