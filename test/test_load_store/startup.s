        .abicalls
        .section        .text.init,"ax",@progbits
        .align  2
        .globl  startup
        .set    nomips16
        .set    nomicromips
        .ent    startup
	.set noat
        .type   startup, @function
startup:
        .frame  $sp,0,$31               # vars= 0, regs= 0/0, args= 0, gp= 0        .mask   0x00000000,0        .fmask  0x00000000,0
	nop
	li $1,42
	nop
	li $2,42
	nop
	li $3,42
	nop
	li $4,42
	nop
	nop
	nop
	j main
	.end	startup
	.size	startup, .-startup
