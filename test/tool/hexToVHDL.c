#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#define DEBUG                   0

int binary(int byte) {
    for (int i = 31; i >= 0; i--){
        if(byte & 1<<i)
            printf("1");
        else
            printf("0");
    }
}

int main(int argc, char** argv){
    string line;
    std::stringstream ss;
    char* substr;
    int length;
    int addr;
    long int byte;
    int type;
    ifstream myfile(argv[1]);

    if(myfile.is_open()){
        while(myfile.good()){
            getline( myfile, line);
            if(DEBUG)
                printf("\n\n-- reading line: %s %s %s %s\n", line.substr(0,3).c_str(), line.substr(3,4).c_str(), line.substr(5,2).c_str(),line.substr(9).c_str());
            length = strtol(line.substr(1,2).c_str(),NULL,16);
            addr = strtol(line.substr(3,4).c_str(),NULL,16)/4;
            type = strtol(line.substr(7,2).c_str(),NULL,16);
            for (int i = 0; i < length*2; i+=8){
                if(i%8 == 0)
                    if(type != 0){
                        printf("\n-- Length:%d; Start: %d\n", length, addr);
                        printf("\n\n-- reading line: %s %s %s %s %s\n", 
                                        line.substr(0,3).c_str(), 
                                        line.substr(3,4).c_str(), 
                                        line.substr(7,2).c_str(),
                                        line.substr(9,line.substr(9).length()-3).c_str(),
                                        line.substr(line.substr(9).length()-2,2).c_str());
                        printf("-- %d: ", type);
                    }else
                        if(DEBUG)
                            printf("\n-- Length:%d; Start: %d\n", length,addr);
                byte = strtol(line.substr(i+9,8).c_str(),NULL,16);
                            printf("\n");

                printf("%d => \"", addr++);
                binary(byte);
                printf("\", ");
            }
            if(type == 1)
                break;
//          printf("\n");
        }
        myfile.close();
    }
}
