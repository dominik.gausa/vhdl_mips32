#!/bin/bash

# git clone git://gcc.gnu.org/git/gcc.git
#  ftp://ftp.gmplib.org/pub/gmp/gmp-5.1.3.tar.bz2
#   http://www.mpfr.org/mpfr-current/mpfr-3.1.2.tar.bz2
#  http://www.multiprecision.org/mpc/download/mpc-1.0.1.tar.gz

#  mv mpfr-3.1.2 gcc/mpfr
#  mv mpc-1.0.1 gcc/mpc
# git lone git://sourceware.org/git/binutils.git
export TARGET="mipsel-unknown-linux-gnu"
echo Target $TARGET
export WDIR=`pwd`/${TARGET}/tmp
export PREFIX=`pwd`/${TARGET}
echo Prefix $PREFIX
export PATH=${PATH}:${PREFIX}/bin
echo PATH $PATH



echo
echo
echo BUILDING BINUTILS
echo
echo

sleep 5

if [ "" = "1" ]
then
cd binutils-2.23
rm -rf build
mkdir -p build && cd build
../configure --target=${TARGET} --prefix=${PREFIX}
make -j9 || exit 1
make install || exit 1
cd ..
fi

echo
echo
echo BUILDING GCC
echo
echo

sleep 5

cd gcc
#rm -rf build
mkdir -p build
cd build
../configure --target=${TARGET} --prefix=${PREFIX} \
  --disable-threads --without-headers \
  --with-newlib --with-gnu-as --with-gnu-ld

make all-gcc -j9 || exit 1

echo
echo
echo INSTALL GCC
echo
echo

make install-gcc || exit 1
