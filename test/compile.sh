#!/bin/bash

g++ tool/hexToVHDL.c -o tool/hexToVHDL

PREFIX=mipsel-unknown-linux-gnu-

function compile {
    TARGET=$1
    cd ${TARGET}
    ${PREFIX}gcc -S ${TARGET}.c -march=mips32r2 -Os
    ${PREFIX}as ${TARGET}.s -march=mips32 -a -o ${TARGET} > ${TARGET}.asm.map
    ${PREFIX}objcopy -O ihex ${TARGET} ${TARGET}.hex
    ../tool/hexToVHDL ${TARGET}.hex > ${TARGET}.vhdl
    cd ..
}


for test_case in test_*
do
    compile $test_case
done