-- Copyright (C) 1991-2012 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 32-bit"
-- VERSION		"Version 12.1 Build 243 01/31/2013 Service Pack 1 SJ Web Edition"
-- CREATED		"Wed May 15 12:05:58 2013"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.work_pkg.all;


ENTITY ram IS 
	port (IN_CLK		: in STD_LOGIC;
			IN_RESET		: in STD_LOGIC;
			IN_ADDRESS	: in std_logic_vector(31 downto 0);
			IN_ADDRESS_R	: in STD_LOGIC;
			IN_ADDRESS_W	: in std_logic_vector(1 downto 0);
			io_DATA	: inout STD_LOGIC_VECTOR (31 downto 0)
			);
END ram;

ARCHITECTURE bdf_type OF ram IS 
	
	signal s_RAM			: t_32BitArray(0 to RAM_SIZE-1);
BEGIN 

	process (IN_CLK)
	begin
		if rising_edge(IN_CLK) 
			AND IN_ADDRESS(31 downto 30) = "01" -- RAM
		then
			io_DATA <=(others => 'Z');
			if IN_ADDRESS_W /= "00" then
				s_RAM(to_integer(unsigned(IN_ADDRESS(15 downto 2)))) <= io_DATA;
			end if;
			if IN_ADDRESS_R = '1' then
				io_DATA <= s_RAM(to_integer(unsigned(IN_ADDRESS(15 downto 2))));
			end if;
		end if;
	end process;

END bdf_type;