LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

LIBRARY work;
use work.work_pkg.all;
USE work.basic_pkg.all;



ENTITY clk_div IS
	port (IN_CLK: in STD_LOGIC;
			IN_RESET: in STD_LOGIC;
			OUT_CLK: out STD_LOGIC
			);
END clk_div;

architecture arch of clk_div is
	SIGNAL   s_CLK_DIV: STD_LOGIC_VECTOR(16 downto 0);
begin

	process (IN_CLK,
				IN_RESET)
	begin
		if IN_RESET = '1' then
			s_CLK_DIV <= (others => '0');
		else
			if CLK_DIV_SYS = CLK_DIV_1 then
				OUT_CLK <= IN_CLK;
			elsif rising_edge(IN_CLK) then
				s_CLK_DIV <= std_LOGIC_VECTOR(unsigned(s_CLK_DIV)+1);
				OUT_CLK <= s_CLK_DIV(CLK_DIV_SYS-1);
			end if;
		end if;
	end process;
	
end architecture;