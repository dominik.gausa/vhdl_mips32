LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.work_pkg.all;


entity top_tb IS
end entity;

architecture arch of top_tb is


	signal s_CLK	: std_logic := '0';
	signal s_RESET	: std_logic := '0';
	signal s_KEY	:  std_logic_vector(1 downto 0) := (others => '0');

	signal GPIO_0: STD_LOGIC_VECTOR(0 to 33) := (others => '0');
	signal GPIO_0_IN: STD_LOGIC_VECTOR(0 to 1) := (others => '0');
	signal GPIO_1: STD_LOGIC_VECTOR(0 to 33) := (others => '0');
	signal GPIO_1_IN: STD_LOGIC_VECTOR(0 to 1) := (others => '0');
	signal SWITCH: STD_LOGIC_VECTOR(0 to 3) := (others => '0');
	signal LED: STD_LOGIC_VECTOR(0 to 7) := (others => '0');

	signal out_ADC_CS_N: STD_LOGIC;
	signal out_ADC_SCLK: STD_LOGIC;
	signal out_ADC_SADDR: STD_LOGIC;
	signal in_ADC_SDAT: STD_LOGIC;
begin

	process
	begin
		GPIO_0(3 to 15) <= (others => '0');
		GPIO_0(0 to 2) <= (others => '1');
		wait for 6 us;
		GPIO_0(0 to 2) <= (others => '0');
		
		while 1 = 1 loop
			wait for 10 ns;
		end loop;
	end process;

	process
	begin
		s_RESET <= '0';
		s_CLK <= '0';
		s_RESET <= '1';
		while 1 = 1 loop
			s_CLK   <= not s_CLK;
			wait for 10 ns;
		end loop;
	end process;
	
	s_KEY(0) <= s_RESET;
	
	top: entity work.top 
		port map(CLK => s_CLK,
					in_KEY => s_KEY,
					io_GPIO_0 => GPIO_0,
					in_GPIO_0_IN => GPIO_0_IN,
					io_GPIO_1 => GPIO_1,
					in_GPIO_1_IN => GPIO_1_IN,
					in_SWITCH => SWITCH,
					out_LED => LED,
					out_ADC_CS_N => out_ADC_CS_N,
					out_ADC_SCLK => out_ADC_SCLK,
					out_ADC_SADDR => out_ADC_SADDR,
					in_ADC_SDAT => in_ADC_SDAT
					);
end architecture;

