LIBRARY ieee;
USE ieee.std_logic_1164.all;
-- USE ieee.std_logic_unsigned.all;

package cpu_pkg is
	
	type type_FORWARD is (e_REG, e_ALU_R, e_ALU_LAST);

	
	type t_BUFFER_IF_ID is record
		a_OP       		: std_logic_vector(31 downto 0);
		a_PC       		: std_logic_vector(31 downto 0);
	end record;

	type t_BUFFER_ID_EXE is record
		a_PC     	 	: std_logic_vector(31 downto 0);
		
		a_OP				: std_logic_vector(5 downto 0);
		
		a_Rs				: std_logic_vector(4 downto 0);
		a_Rt				: std_logic_vector(4 downto 0);
		a_Rd				: std_logic_vector(4 downto 0);
		a_shamt			: std_logic_vector(4 downto 0);
		a_ALUOP			: std_logic_vector(5 downto 0);
		
		a_Immediate		: std_logic_vector(31 downto 0);
		
		a_A				: std_logic_vector(31 downto 0);
		a_B				: std_logic_vector(31 downto 0);
		
		a_MemWrite		: std_logic_vector(1 downto 0);
		a_MemRead		: std_logic_vector(1 downto 0);
		a_RegDst			: std_logic;								-- 1: Rd 0: Rt
		
--		a_Branch			: t_BRANCH; todo
		a_Branch			: std_logic;
		a_WB				: std_logic;
		a_LINK			: std_logic;
		a_signed			: std_logic;
	end record;
	
	type t_BUFFER_EXE_MEM is record
		a_PC     	 	: std_logic_vector(31 downto 0);
		a_MemRead		: std_logic_vector(1 downto 0);
		a_MemWrite		: std_logic_vector(1 downto 0);
		a_LINK			: std_logic;
		
		a_Branch			: std_logic;
		a_WB				: std_logic;
		a_RegWrite     : std_logic_vector(4 downto 0);
		a_PC_New			: std_logic_vector(31 downto 0);
		a_ALURet			: std_logic_vector(31 downto 0);
		a_B				: std_logic_vector(31 downto 0);
		a_Rt				: std_logic_vector(4 downto 0);
		
		a_signed			: std_logic;
	end record;
	
	type t_BUFFER_MEM_WB is record
		a_PC     	 	: std_logic_vector(31 downto 0);
		a_WB				: std_logic;
		a_LINK			: std_logic;
		a_MemRead 		: std_logic_vector(1 downto 0);
		a_RegWrite     : std_logic_vector(4 downto 0);
		a_ALURet			: std_logic_vector(31 downto 0);
		a_MemData		: std_logic_vector(31 downto 0);
	end record;

end cpu_pkg;
