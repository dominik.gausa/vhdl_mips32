LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.cpu_pkg.all;


entity cpu_alu IS
		port (
				IN_CLK	: in	std_logic;
				IN_OP		: in  std_logic_vector(5 downto 0);
				IN_SA		: in  std_logic_vector(4 downto 0);
				IN_A		: in  std_logic_vector(31 downto 0);
				IN_B		: in  std_logic_vector(31 downto 0);
				
				OUT_R		: out  std_logic_vector(31 downto 0);
				OUT_FLAG	: out  std_logic_vector(7 downto 0)
				);
end entity;

architecture arch of cpu_alu is
	signal s_CARRY : std_logic := '0';
begin
	
	process (IN_OP, IN_CLK,IN_A,IN_B, s_CARRY)
		variable v_A : std_logic_vector(32 downto 0);
		variable v_B : std_logic_vector(32 downto 0);
		variable v_R : std_logic_vector(32 downto 0);
		variable v_CARRY : std_logic := '0';
		variable v_shift : integer := 0;
	begin
--		if rising_edge(IN_CLK) then
			v_CARRY := '0';
			v_shift := to_integer(unsigned(IN_SA));
			OUT_R <= (others => '0');
			case IN_OP is
				when "000000" => -- SLL
					OUT_R <= (others => '0');
					OUT_R(31 downto v_shift) <= IN_B(31-v_shift downto 0);
				when "000010" => -- SRL
					OUT_R <= (others => '0');
					OUT_R(31-v_shift downto 0) <= IN_B(31 downto v_shift);
				when "100001" => -- ADD
					v_A := "0" & IN_A;
					v_B := "0" & IN_B;
					v_R := std_logic_vector(unsigned(v_A) + unsigned(v_B));
					OUT_R <= v_R(31 downto 0);
					v_CARRY := v_R(32);
					
				when "100011" => -- SUB
					v_A := "0" & IN_A;
					v_B := "0" & IN_B;
					v_R := std_logic_vector(unsigned(v_A) - unsigned(v_B));
					OUT_R <= v_R(31 downto 0);
					v_CARRY := v_R(32);
					
				when "100100" => -- AND
					v_A := "0" & IN_A;
					v_B := "0" & IN_B;
					v_R := v_A AND v_B;
					OUT_R <= v_R(31 downto 0);

				when "100101" => -- OR
					v_A := "0" & IN_A;
					v_B := "0" & IN_B;
					v_R := v_A OR v_B;
					OUT_R <= v_R(31 downto 0);
					
				when "100110" => -- XOR
					v_A := "0" & IN_A;
					v_B := "0" & IN_B;
					v_R := v_A XOR v_B;
					OUT_R <= v_R(31 downto 0);
					
				when "100111" => -- NOR
					v_A := "0" & IN_A;
					v_B := "0" & IN_B;
					v_R := v_A NOR v_B;
					OUT_R <= v_R(31 downto 0);
				when others =>
			end case;
			
			if v_CARRY = '1' then
				s_CARRY <= '0';
			end if;
--		end if;
	end process;
	
end architecture;


