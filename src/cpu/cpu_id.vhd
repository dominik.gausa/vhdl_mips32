LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.cpu_pkg.all;

entity cpu_id IS
	port (
				IN_CLK		: in	std_logic;
				IN_RESET		: in	std_logic;
				IN_REG		: in t_32BitArray(0 to 31);
				IN_PIPE_IF_ID 		: in t_BUFFER_IF_ID;
				OUT_PIPE_ID_EXE 		: out t_BUFFER_ID_EXE;
				OUT_bubble : out std_logic
			);
end entity;



architecture arch of cpu_id is
	signal s_bubble : std_logic := '0';
	signal s_PIPE_ID_EXE 		: t_BUFFER_ID_EXE;
	signal NOP 		: t_BUFFER_ID_EXE;
begin

	
	-- -----------------------------
	-- DECODE & Register
	-- -----------------------------
	process (IN_CLK,
				IN_RESET,
				IN_PIPE_IF_ID,
				IN_REG)
		variable v_PIPE_ID_EXE : t_BUFFER_ID_EXE;
	begin
		if IN_RESET = '1'  then
			NOP.a_MemWrite <= "00";
			NOP.a_MemRead <= "00";
			NOP.a_WB <= '0';
			NOP.a_LINK <= '0';
			NOP.a_Branch <= '0';
			NOP.a_RegDst <= '0';
			NOP.a_immediate <= (others => '0');

			NOP.a_OP <= (others => '0');
			NOP.a_PC <= (others => '0');

			NOP.a_Rs <= (others => '0');
			NOP.a_Rt <= (others => '0');
			NOP.a_Rd <= (others => '0');

			NOP.a_shamt <= (others => '0');
			NOP.a_ALUOP <= (others => '0');
			
			NOP.a_A <= (others => '0');
			NOP.a_B <= (others => '0');
			
		else
			if rising_edge(IN_CLK) then
				if s_BUBBLE = '1' then
					s_BUBBLE <= '0';
				else
					s_PIPE_ID_EXE.a_PC <= IN_PIPE_IF_ID.a_PC;
					NOP.a_PC <= IN_PIPE_IF_ID.a_PC;
					-- -----------------------------
					-- RESET
					-- -----------------------------
					
					s_PIPE_ID_EXE.a_MemWrite <= "00";
					s_PIPE_ID_EXE.a_MemRead <= "00";
					s_PIPE_ID_EXE.a_WB <= '1';
					s_PIPE_ID_EXE.a_Branch <= '0';
					s_PIPE_ID_EXE.a_RegDst <= '0';
					s_PIPE_ID_EXE.a_LINK <= '0';

					v_PIPE_ID_EXE.a_OP := IN_PIPE_IF_ID.a_OP(31 downto 26);
					v_PIPE_ID_EXE.a_Rs := IN_PIPE_IF_ID.a_OP(25 downto 21);
					v_PIPE_ID_EXE.a_Rt := IN_PIPE_IF_ID.a_OP(20 downto 16);
					v_PIPE_ID_EXE.a_Rd := IN_PIPE_IF_ID.a_OP(15 downto 11);

					s_PIPE_ID_EXE.a_OP <= IN_PIPE_IF_ID.a_OP(31 downto 26);
					s_PIPE_ID_EXE.a_Rs <= IN_PIPE_IF_ID.a_OP(25 downto 21);
					s_PIPE_ID_EXE.a_Rt <= IN_PIPE_IF_ID.a_OP(20 downto 16);
					s_PIPE_ID_EXE.a_Rd <= IN_PIPE_IF_ID.a_OP(15 downto 11);
					
					s_PIPE_ID_EXE.a_shamt <= IN_PIPE_IF_ID.a_OP(10 downto 6);
					s_PIPE_ID_EXE.a_ALUOP <= IN_PIPE_IF_ID.a_OP(5 downto 0);
					
					s_PIPE_ID_EXE.a_immediate(31 downto 15) <= (others => IN_PIPE_IF_ID.a_OP(15));
					s_PIPE_ID_EXE.a_immediate(14 downto 0) <= IN_PIPE_IF_ID.a_OP(14 downto 0);
					
					s_PIPE_ID_EXE.a_signed <= '1';
					
					
					-- -----------------------------
					-- DECODE
					-- -----------------------------
					case v_PIPE_ID_EXE.a_OP is
					-->  R-Type
						when "000000" =>
							s_PIPE_ID_EXE.a_RegDst <= '1';
							if unsigned(IN_PIPE_IF_ID.a_OP) = 0 then
								s_PIPE_ID_EXE.a_WB <= '0';
							else
								case v_PIPE_ID_EXE.a_ALUOP is
									when "001101" => -- BREAK
									when others =>
								end case;
							end if;
							
					--> I-Type
							
						when "000001" => -- BGEZ
							-- BGEZ 		00001 Gt or EQ 0
							-- BGEZAL 	10001 Gt or EQ 0 Link
							-- BGEZALL 	10011 Gt or EQ 0 Link Likely
							-- BGEZL 	00011 Gt or EQ 0 Likely
							-- BLTZ 		00000 Lt 0
							-- BLTZAL 	10000 Lt 0 Link
							-- BLTZALL 	10010 Lt 0 Link Likely
							-- BLTZL 	00010 Lt 0 Likely
							--------------------------------------
							-- Lnk -  -  Like GT+EQ
							s_PIPE_ID_EXE.a_WB <= '0';
							s_PIPE_ID_EXE.a_LINK <= IN_PIPE_IF_ID.a_OP(20);
							s_PIPE_ID_EXE.a_Branch <= '1';
							
						when "000010" => -- J
							s_PIPE_ID_EXE.a_WB <= '0';
							
						when "000011" => -- JAL
							s_PIPE_ID_EXE.a_LINK <= '1';
							s_PIPE_ID_EXE.a_Rt <= "11111";
							
						
						when "000100" | "010100"=> -- BEQ(L)
							s_PIPE_ID_EXE.a_LINK <= v_PIPE_ID_EXE.a_OP(1);
							s_PIPE_ID_EXE.a_WB <= '0';
							s_PIPE_ID_EXE.a_Branch <= '1';

						when "000101" => -- BNE
							s_PIPE_ID_EXE.a_WB <= '0';
							s_PIPE_ID_EXE.a_Branch <= '1';

						when "000110" | "010110" => -- BLEZ(L)
							
						when "000111" | "010111" => -- BGTZ(L)
						

						when "001001" => -- ADDI
							s_PIPE_ID_EXE.a_ALUOP <= "100001";
							
						when "001100" => -- ANDI
							s_PIPE_ID_EXE.a_immediate <= "00000000" & "00000000" & IN_PIPE_IF_ID.a_OP(15 downto 0);
							s_PIPE_ID_EXE.a_ALUOP <= "100100";
							
						when "001101" => -- ORI
							s_PIPE_ID_EXE.a_immediate <= "00000000" & "00000000" & IN_PIPE_IF_ID.a_OP(15 downto 0);
							s_PIPE_ID_EXE.a_ALUOP <= "100101";

						when "001110" => -- XORI
							s_PIPE_ID_EXE.a_immediate <= "00000000" & "00000000" & IN_PIPE_IF_ID.a_OP(15 downto 0);
							s_PIPE_ID_EXE.a_ALUOP <= "100110";
							
						when "001111" => -- LUI
							s_PIPE_ID_EXE.a_immediate <= IN_PIPE_IF_ID.a_OP(15 downto 0) & "00000000" & "00000000";
							s_PIPE_ID_EXE.a_ALUOP <= "100001";

						when "100000" => -- LB
							s_PIPE_ID_EXE.a_MemRead <= "01";
							s_PIPE_ID_EXE.a_ALUOP <= "100001";
						when "100001" => -- LH
							s_PIPE_ID_EXE.a_MemRead <= "10";
							s_PIPE_ID_EXE.a_ALUOP <= "100001";
						when "100011" => -- LW
							s_PIPE_ID_EXE.a_MemRead <= "11";
							s_PIPE_ID_EXE.a_ALUOP <= "100001";				
						when "100100" => -- LBU
							s_PIPE_ID_EXE.a_MemRead <= "01";
							s_PIPE_ID_EXE.a_ALUOP <= "100001";
							s_PIPE_ID_EXE.a_signed <= '0';
						when "100101" => -- LHU
							s_PIPE_ID_EXE.a_MemRead <= "10";
							s_PIPE_ID_EXE.a_ALUOP <= "100001";
							s_PIPE_ID_EXE.a_signed <= '0';
							
						when "101000" => -- SB
							s_PIPE_ID_EXE.a_WB <= '0';
							s_PIPE_ID_EXE.a_MemWrite <= "01";
							s_PIPE_ID_EXE.a_ALUOP <= "100001";
						when "101001" => -- SH
							s_PIPE_ID_EXE.a_WB <= '0';
							s_PIPE_ID_EXE.a_MemWrite <= "10";
							s_PIPE_ID_EXE.a_ALUOP <= "100001";
						when "101011" => -- SW
							s_PIPE_ID_EXE.a_WB <= '0';
							s_PIPE_ID_EXE.a_MemWrite <= "11";
							s_PIPE_ID_EXE.a_ALUOP <= "100001";
						
						when others =>
					end case;
					
					
					
					-- Data-Hazzard-Detection Memory
					if s_PIPE_ID_EXE.a_MemRead /= "00" then
						if IN_PIPE_IF_ID.a_OP(25 downto 21) = s_PIPE_ID_EXE.a_Rt then
							s_BUBBLE <= '1';
						end if;
						if IN_PIPE_IF_ID.a_OP(20 downto 16) = s_PIPE_ID_EXE.a_Rt then
							s_BUBBLE <= '1';
						end if;
					end if;
					
				end if;

			end if;
		end if;
	end process;

	OUT_bubble <= s_BUBBLE;

	process(IN_PIPE_IF_ID,
				IN_REG,
				s_PIPE_ID_EXE)
	begin
		if IN_RESET = '0' then
			s_PIPE_ID_EXE.a_A <= IN_REG(to_integer(unsigned(s_PIPE_ID_EXE.a_Rs)));
			s_PIPE_ID_EXE.a_B <= IN_REG(to_integer(unsigned(s_PIPE_ID_EXE.a_Rt)));
		end if;
	end process;
	
	process(IN_PIPE_IF_ID,
				IN_REG,
				s_PIPE_ID_EXE)
	begin
		if s_BUBBLE = '0' and IN_RESET = '0' then
			OUT_PIPE_ID_EXE <= s_PIPE_ID_EXE;
		else
			OUT_PIPE_ID_EXE <= NOP;
		end if;
	end process;
	
end architecture;

