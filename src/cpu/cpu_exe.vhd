LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.cpu_pkg.all;


entity cpu_exe IS
	port (
				IN_CLK		: in	std_logic;
				IN_RESET		: in	std_logic;
				
				IN_PIPE_ID_EXE 		: t_BUFFER_ID_EXE;
				OUT_PIPE_EXE_MEM 	: t_BUFFER_EXE_MEM
			);
end entity;



architecture arch of cpu_exe is

		-- -------------------
		-- ALU
		-- -------------------
		signal s_ALURet 		: std_logic_vector(31 downto 0)	:= (others => '0');
		
begin


end architecture;



