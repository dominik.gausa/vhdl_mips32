LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.cpu_pkg.all;

entity cpu IS
	port (
				IN_CLK			: in	std_logic;
				IN_RESET			: in	std_logic;
				IN_Prog_CLK 	: in  std_logic;
				IN_PROG_DATA	: in  std_logic;
				OUT_PC		: OUT STD_LOGIC_VECTOR (31 downto 0);
				OUT_ADDRESS 	: out std_logic_vector(31 downto 0);
				OUT_ADDRESS_R	: out std_logic;
				OUT_ADDRESS_W 	: out std_logic_vector(1 downto 0);
				io_DATA			: inout std_logic_vector(31 downto 0)
			);
end entity;



architecture arch of cpu is



		signal s_ROM	: t_32BitArray(0 to 1024) := (

0 => "00100001000100000000000000000000", 
1 => "00000000000100000000010100111100", 
2 => "00001010000000000000001100100100", 
3 => "00100001001000000100010100000000", 
4 => "10000000001000000000010000000000", 
5 => "00000000000000001000001010101100", 
6 => "00000001000000000100001000100100", 
7 => "11111100111111110100001100010100", 
8 => "00100001001000000100010100000000", 
9 => "00000000000000000000000000000000", 
10 => "00000000000100000000001100111100", 
11 => "00101010000000000000010100100100", 
12 => "00000000010000000000010000111100", 
13 => "00000100000000000110011010001100", 
14 => "00000101000000001100000000010000", 
15 => "00000000000000000000000000000000", 
16 => "00000100000000000110010110101100", 
17 => "00000000000000001000001010101100", 
18 => "00001101000000000000000000001000", 
19 => "00000001000000000100001000100100", 
20 => "00010100000000000000000000001000", 
21 => "00000000000000000000000000000000",
					others => (others => '0'));
							


		--:=									(others => (others => '0'));

		
--		Name 	Register Number 	Usage 	Preserved by callee
--		$zero 	0 	hardwired 0 	N/A
--		$v0-$v1 	2-3 	return value and expression evaluation 	no
--		$a0-$a3 	4-7 	arguments 	no
--		$t0-$t7 	8-15 	temporary values 	no
--		$s0-$s7 	16-23 	saved values 	YES
--		$t8-$t9 	24-25 	more temporary values 	no
--		$gp 	28 	global pointer 	YES
--		$sp 	29 	stack pointer 	YES
--		$fp 	30 	frame pointer 	YES
--		$ra 	31 	return address 	YES
		signal s_REG 			: t_32BitArray(0 to 31) 			:= (
																					0 => std_logic_vector(to_unsigned(0,32)),
																					1 => std_logic_vector(to_unsigned(1,32)),
																					2 => std_logic_vector(to_unsigned(2,32)),
																					3 => std_logic_vector(to_unsigned(3,32)),
																					4 => std_logic_vector(to_unsigned(4,32)),
																					5 => std_logic_vector(to_unsigned(5,32)),
																					6 => std_logic_vector(to_unsigned(6,32)),
																					7 => std_logic_vector(to_unsigned(7,32)),
																					8 => std_logic_vector(to_unsigned(8,32)),
																					9 => std_logic_vector(to_unsigned(9,32)),
																					28 => std_logic_vector(to_unsigned(0,32)),
																					29 => std_logic_vector(to_unsigned(RAM_END,32)),
																					30 => std_logic_vector(to_unsigned(0,32)),
																					others => (others=>'0'));
		
		-- -------------------
		-- ALU
		-- -------------------
		signal s_ALU_A 		: std_logic_vector(31 downto 0)	:= (others => '0');
		signal s_ALU_B 		: std_logic_vector(31 downto 0)	:= (others => '0');
		signal s_ALU_pre_B 	: std_logic_vector(31 downto 0)	:= (others => '0');
		signal s_ALUSrcA 		: type_FORWARD	:= e_REG;
		signal s_ALUSrcB 		: type_FORWARD	:= e_REG;
		signal s_ALURet 		: std_logic_vector(31 downto 0)	:= (others => '0');

		
		-- BUFFER
		signal s_PIPE_IF_ID 		: t_BUFFER_IF_ID;
		signal s_PIPE_ID_EXE 		: t_BUFFER_ID_EXE;
		signal s_PIPE_EXE_MEM 	: t_BUFFER_EXE_MEM;
		signal s_PIPE_MEM_WB 		: t_BUFFER_MEM_WB;
		
		signal s_bubble : std_logic := '0';
		
		
		signal s_counter : std_logic_vector(31 downto 0) := (others => '0');
		signal s_data_read : std_logic_vector(31 downto 0) := (others => '0');
		
		signal s_Branch_A : std_logic_vector(31 downto 0) := (others => '0');
		signal s_Branch_B : std_logic_vector(31 downto 0) := (others => '0');
begin

	-- Programming
--	process (IN_CLK,IN_RESET,IN_Prog_CLK)
--		variable v_mem_val : std_logic_vector(31 downto 0) := (others => '0');
--	begin
--		if IN_RESET = '1' then
--		else
--		end if;
--	end process;

	
	
	-- -----------------------------
	-- FETCH
	-- -----------------------------
	process (IN_CLK, IN_RESET)
		variable v_PC : std_logic_vector(31 downto 0);
		variable v_PC_new : std_logic_vector(31 downto 0);
		variable v_do_branch : std_logic := '0';
		variable v_do_jump : std_logic := '0';
		variable v_jump_target : std_logic_vector(31 downto 0);
	begin
		if IN_RESET = '1' then
			s_PIPE_IF_ID.a_PC <= (others => '0');
			s_PIPE_IF_ID.a_OP <= (others => '0');
		else
			if rising_edge(IN_CLK) then
				-- Jumps
				if v_do_jump = '1' then -- TODO BIG TODO ... this is so TODO that it's even a BUG !!11
					v_PC := v_jump_target;
					v_do_jump := '0';
				elsif s_PIPE_IF_ID.a_OP(31 downto 27) = "00001" then
					v_jump_target := s_PIPE_IF_ID.a_PC(31 downto 28) & s_PIPE_IF_ID.a_OP(25 downto 0) & "00";
					v_do_jump := '1';
					v_PC := std_logic_vector(unsigned(s_PIPE_IF_ID.a_PC)+4);
				elsif s_PIPE_IF_ID.a_OP(31 downto 26) & s_PIPE_IF_ID.a_OP(20 downto 16) & s_PIPE_IF_ID.a_OP(5 downto 1) = "0000000000000100" then
					v_jump_target := s_REG(to_integer(unsigned(s_PIPE_IF_ID.a_OP(25 downto 21))));
					v_do_jump := '1';
					v_PC := std_logic_vector(unsigned(s_PIPE_IF_ID.a_PC)+4);
				else	
				-- Branches	
					v_PC_New := s_PIPE_ID_EXE.a_Immediate(29 downto 0) & "00";
					-- debug
					s_Branch_A <= s_ALU_A;
					s_Branch_B <= s_ALU_B;
					if s_bubble = '0' then
						-- BRANCH DETECTION
						-- Comparator for Branch >= < 0
						v_do_branch := '0';
						if s_PIPE_ID_EXE.a_Branch = '1' then
							if s_PIPE_ID_EXE.a_OP(2) = '0' then
								if s_PIPE_ID_EXE.a_Rt(0) = '0' then
									if to_integer(unsigned(s_ALU_A)) >= 0 then
										v_do_branch := '1';
									end if;
								elsif s_PIPE_ID_EXE.a_Rt(0) = '1' then
									if to_integer(unsigned(s_ALU_A)) < 0 then
										v_do_branch := '1';
									end if;
								end if;
							elsif s_PIPE_ID_EXE.a_OP(2) = '1' then
								if s_PIPE_ID_EXE.a_OP(1) = '0' then -- EQ or NEQ
									if s_ALU_A = s_ALU_pre_B then
										if s_PIPE_ID_EXE.a_OP(0) = '0' then
											v_do_branch := '1';
										end if;
									else
										if s_PIPE_ID_EXE.a_OP(0) = '1' then
											v_do_branch := '1';
										end if;
									end if;
								elsif s_PIPE_ID_EXE.a_OP(1) = '1' then -- LET or GTZ
									if to_integer(unsigned(s_ALU_A)) > 0 then
										if s_PIPE_ID_EXE.a_OP(0) = '0' then
											v_do_branch := '1';
										end if;
									else
										if s_PIPE_ID_EXE.a_OP(0) = '1' then
											v_do_branch := '1';
										end if;
									end if;
								end if;
							end if;
						end if;
						-- END BRANCH DETECTION
						
						
						if v_do_branch = '1' then
							v_PC := std_logic_vector(unsigned(v_PC_New) + unsigned(s_PIPE_ID_EXE.a_PC));
							-- v_PC := std_logic_vector(unsigned(s_PIPE_EXE_MEM.a_PC_New)+4);
						else 
							v_PC := std_logic_vector(unsigned(s_PIPE_IF_ID.a_PC)+4);
						end if;
					end if;
				end if;
				--if v_PC(1 downto 0) = "00" then
				s_PIPE_IF_ID.a_OP(31 downto 0) <= --s_RAM(to_integer(unsigned(v_PC(31 downto 2))));
										s_ROM(to_integer(unsigned(v_PC(31 downto 2))))(7 downto 0)
										& s_ROM(to_integer(unsigned(v_PC(31 downto 2))))(15 downto 8)
										& s_ROM(to_integer(unsigned(v_PC(31 downto 2))))(23 downto 16)
										& s_ROM(to_integer(unsigned(v_PC(31 downto 2))))(31 downto 24);
--				else
--					s_PIPE_IF_ID.a_OP <= (others => '0');
--				end if;
				
					
				s_PIPE_IF_ID.a_PC <= v_PC;
				OUT_PC <= s_ROM(to_integer(unsigned(v_PC(31 downto 2))))(7 downto 0)
										& s_ROM(to_integer(unsigned(v_PC(31 downto 2))))(15 downto 8)
										& s_ROM(to_integer(unsigned(v_PC(31 downto 2))))(23 downto 16)
										& s_ROM(to_integer(unsigned(v_PC(31 downto 2))))(31 downto 24);
			end if;
		end if;
	end process;
					
						
						
		-- -----------------------------
		-- ALU
		-- -----------------------------
		process (IN_CLK,IN_RESET)
			variable v_PC_New	:	std_logic_vector(31 downto 0);
			variable v_RegWrite	:	std_logic_vector(4 downto 0);
			
		begin
			if IN_RESET ='1' then
					s_PIPE_EXE_MEM.a_WB <= '0';
					s_PIPE_EXE_MEM.a_PC <= (others => '0');
					s_PIPE_EXE_MEM.a_LINK <= '0';
					s_PIPE_EXE_MEM.a_signed <= '0';
					s_PIPE_EXE_MEM.a_MemRead <= (others => '0');
					s_PIPE_EXE_MEM.a_MemWrite <= (others => '0');
					s_PIPE_EXE_MEM.a_Branch <= '0';
					s_PIPE_EXE_MEM.a_Rt <= (others => '0');
					s_PIPE_EXE_MEM.a_ALURet <= (others => '0');
					s_PIPE_EXE_MEM.a_B <= (others => '0');
										
					s_PIPE_EXE_MEM.a_RegWrite <= (others => '0');

			else
				if rising_edge(IN_CLK) then
					s_PIPE_EXE_MEM.a_WB <= s_PIPE_ID_EXE.a_WB;
					s_PIPE_EXE_MEM.a_PC <= std_logic_vector(unsigned(s_PIPE_ID_EXE.a_PC)+8);
					s_PIPE_EXE_MEM.a_PC_New <= std_logic_vector(unsigned(s_PIPE_ID_EXE.a_PC)+8);
					s_PIPE_EXE_MEM.a_LINK <= s_PIPE_ID_EXE.a_LINK;
					s_PIPE_EXE_MEM.a_signed <= s_PIPE_ID_EXE.a_signed;
					s_PIPE_EXE_MEM.a_MemRead <= s_PIPE_ID_EXE.a_MemRead;
					s_PIPE_EXE_MEM.a_MemWrite <= s_PIPE_ID_EXE.a_MemWrite;
					s_PIPE_EXE_MEM.a_Branch <= s_PIPE_ID_EXE.a_Branch;
					s_PIPE_EXE_MEM.a_Rt <= s_PIPE_ID_EXE.a_Rt;
					s_PIPE_EXE_MEM.a_ALURet <= s_ALURet;
					s_PIPE_EXE_MEM.a_B <= s_PIPE_ID_EXE.a_B;
					
					
					if s_PIPE_ID_EXE.a_RegDst = '1' then
						v_RegWrite := s_PIPE_ID_EXE.a_Rd;
					else
						v_RegWrite := s_PIPE_ID_EXE.a_Rt;
					end if;
					s_PIPE_EXE_MEM.a_RegWrite <= v_RegWrite;
					
				end if;
			end if;
		end process;
						
							
	-- -----------------------------
	-- MEMORY
	-- -----------------------------
	process (IN_CLK,IN_RESET,s_PIPE_MEM_WB, s_PIPE_EXE_MEM, IN_Prog_CLK,IN_PROG_DATA)
		variable v_write_value : std_logic_vector(31 downto 0) := (others => '0');
		variable v_value_to_write : std_logic_vector(31 downto 0) := (others => '0');
		variable v_need_mem_write : std_logic := '0';
		variable v_mem_val : std_logic_vector(31 downto 0) := (others => '0');
	begin
		if IN_RESET = '1' then

			s_PIPE_MEM_WB.a_PC <= (others => '0');
			s_PIPE_MEM_WB.a_LINK <= '0';
			s_PIPE_MEM_WB.a_ALURet <= (others => '0');
			s_PIPE_MEM_WB.a_RegWrite <= (others => '0');
			s_PIPE_MEM_WB.a_MemRead <= (others => '0');

			s_PIPE_MEM_WB.a_WB <= '0';

		else
			if rising_edge(IN_CLK) then
				s_PIPE_MEM_WB.a_PC <= s_PIPE_EXE_MEM.a_PC;
				s_PIPE_MEM_WB.a_LINK <= s_PIPE_EXE_MEM.a_LINK;
				s_PIPE_MEM_WB.a_ALURet <= s_PIPE_EXE_MEM.a_ALURet;
				s_PIPE_MEM_WB.a_RegWrite <= s_PIPE_EXE_MEM.a_RegWrite;
				s_PIPE_MEM_WB.a_MemRead <= s_PIPE_EXE_MEM.a_MemRead;

				-- Ensure no WB on Mem Write
				if s_PIPE_EXE_MEM.a_MemWrite = "00" then
					s_PIPE_MEM_WB.a_WB <= s_PIPE_EXE_MEM.a_WB;
				else
					s_PIPE_MEM_WB.a_WB <= '0';
				end if;
				
				OUT_ADDRESS <= s_PIPE_EXE_MEM.a_ALURet;
				io_DATA <= (others => 'Z');
				OUT_ADDRESS_R <= s_PIPE_EXE_MEM.a_MemRead(1) or s_PIPE_EXE_MEM.a_MemRead(0);
				OUT_ADDRESS_W <= s_PIPE_EXE_MEM.a_MemWrite;

				if s_PIPE_EXE_MEM.a_MemWrite = "00" then
					io_DATA <= (others => 'Z') ;
				else 
					-- Do Forwarding
					if s_PIPE_MEM_WB.a_RegWrite = s_PIPE_EXE_MEM.a_Rt then
						if s_PIPE_MEM_WB.a_MemRead = "00" then
							io_DATA <= s_PIPE_MEM_WB.a_ALURet;
						else
							io_DATA <= s_PIPE_MEM_WB.a_MemData;
						end if;
					else
						io_DATA <= s_PIPE_EXE_MEM.a_B;
					end if;
				end if;
			end if;
		end if;
	end process;	
				
	process(io_DATA,
				s_PIPE_EXE_MEM.a_MemRead )
	begin
		case s_PIPE_EXE_MEM.a_MemRead is
			when "00" =>
			when "01" => --LB
				s_PIPE_MEM_WB.a_MemData(31 downto 8) <= (others => '0');
				s_PIPE_MEM_WB.a_MemData(7 downto 0) <= io_DATA(to_integer(unsigned(s_PIPE_MEM_WB.a_ALURet(1 downto 0)))*8 + 7 downto to_integer(unsigned(s_PIPE_MEM_WB.a_ALURet(1 downto 0)))*8);
			when "10" => --LH
				s_PIPE_MEM_WB.a_MemData(31 downto 16) <= (others => '0');
				s_PIPE_MEM_WB.a_MemData(15 downto 0) <= io_DATA(to_integer(unsigned(s_PIPE_MEM_WB.a_ALURet(1 downto 0)))*16 + 15 downto to_integer(unsigned(s_PIPE_MEM_WB.a_ALURet(1 downto 0)))*16);
			when "11" => --LW
				s_PIPE_MEM_WB.a_MemData(31 downto 0) <= io_DATA;
			when others=>
		end case;
	end process;

	-- -----------------------------
	-- WRITEBACK
	-- -----------------------------
	process (IN_CLK,IN_RESET,s_PIPE_MEM_WB )
		variable v_read_value : std_logic_vector(31 downto 0) := (others => '0');
	begin
		if IN_RESET = '1' then
		else
			if rising_edge(IN_CLK) then
							-- Read
				v_read_value := (others => '0');
				case s_PIPE_EXE_MEM.a_MemRead is
					when "00" =>
					when "01" => --LB
						v_read_value(7 downto 0) := io_DATA(to_integer(unsigned(s_PIPE_MEM_WB.a_ALURet(1 downto 0)))*8 + 7 downto to_integer(unsigned(s_PIPE_MEM_WB.a_ALURet(1 downto 0)))*8);
						
					when "10" => --LH
						v_read_value(15 downto 0) := io_DATA(to_integer(unsigned(s_PIPE_MEM_WB.a_ALURet(1 downto 0)))*16 + 15 downto to_integer(unsigned(s_PIPE_MEM_WB.a_ALURet(1 downto 0)))*16);
					when "11" => --LW
						v_read_value(31 downto 0) := io_DATA;
					when others=>
				end case;
				
				if s_PIPE_MEM_WB.a_LINK = '0' then
					if unsigned(s_PIPE_MEM_WB.a_RegWrite) = 0 then
						-- don't touch $zero!
					else
						if s_PIPE_MEM_WB.a_WB = '1' then
							if s_PIPE_MEM_WB.a_MemRead = "00" then
								s_REG(to_integer(unsigned(s_PIPE_MEM_WB.a_RegWrite))) <= s_PIPE_MEM_WB.a_ALURet;
							else
								s_REG(to_integer(unsigned(s_PIPE_MEM_WB.a_RegWrite))) <= v_read_value;
							end if;
						end if;
						
					end if;
				-- link
				else
					if unsigned(s_PIPE_MEM_WB.a_RegWrite) = 0 then
						s_REG(31) <= s_PIPE_ID_EXE.a_PC; 
					else
						s_REG(to_integer(unsigned(s_PIPE_MEM_WB.a_RegWrite))) <= s_PIPE_MEM_WB.a_PC;
					end if;
				end if;
			end if;
		end if;
	end process;
	
	
	-- MUX
	-- Forwarding Unit
	process (s_PIPE_ID_EXE,
				s_PIPE_EXE_MEM,
				s_PIPE_MEM_WB, 
				s_ALUSrcA, 
				s_ALUSrcB)
	begin
		s_ALUSrcA <= e_REG;
		if unsigned(s_PIPE_ID_EXE.a_Rs) = 0 then
		else
			if s_PIPE_ID_EXE.a_Rs = s_PIPE_MEM_WB.a_RegWrite then
				if s_PIPE_MEM_WB.a_WB = '1' then -- only if we change it
					s_ALUSrcA <= e_ALU_LAST;
				end if;
			end if;
			if s_PIPE_ID_EXE.a_Rs = s_PIPE_EXE_MEM.a_RegWrite then
				if s_PIPE_EXE_MEM.a_WB = '1' then -- only if we change it
					s_ALUSrcA <= e_ALU_R;
				end if;
			end if;
		end if;
		
		s_ALUSrcB <= e_REG;
		if unsigned(s_PIPE_ID_EXE.a_Rt) = 0 then
		else
			if s_PIPE_ID_EXE.a_Rt = s_PIPE_MEM_WB.a_RegWrite then
				if s_PIPE_MEM_WB.a_WB = '1' then -- only if we change it
					s_ALUSrcB <= e_ALU_LAST;
				end if;
			end if;
			if s_PIPE_ID_EXE.a_Rt = s_PIPE_EXE_MEM.a_RegWrite then
				if s_PIPE_EXE_MEM.a_WB = '1' then -- only if we change it
					s_ALUSrcB <= e_ALU_R;
				end if;
			end if;
		end if;
	end process;
	
	
	-- MUX
	-- Forwarding for ALU A
	process (s_ALUSrcA,
				s_ALU_A,
				s_PIPE_ID_EXE,
				s_PIPE_EXE_MEM,
				io_DATA,
				s_PIPE_MEM_WB)
	begin
		case s_ALUSrcA is
			when e_REG =>
				s_ALU_A <= s_PIPE_ID_EXE.a_A;
			when e_ALU_R =>
				s_ALU_A <= s_PIPE_EXE_MEM.a_ALURet;
			when e_ALU_LAST =>
				if s_PIPE_MEM_WB.a_MemRead = "00" then
					s_ALU_A <= s_PIPE_MEM_WB.a_ALURet;
				else
					s_ALU_A <= io_DATA; --s_PIPE_MEM_WB.a_MemData;
				end if;
			when others =>
		end case;
	end process;

	
	-- MUX
	-- Forwarding for ALU B
	process (s_ALUSrcB,
				s_ALU_pre_B,
				s_PIPE_ID_EXE,
				s_PIPE_EXE_MEM,
				io_DATA,
				s_PIPE_MEM_WB)
	begin
			case s_ALUSrcB is
				when e_REG =>
					s_ALU_pre_B <= s_PIPE_ID_EXE.a_B;
				when e_ALU_R =>
					s_ALU_pre_B <= s_PIPE_EXE_MEM.a_ALURet;
				when e_ALU_LAST =>
					if s_PIPE_MEM_WB.a_MemRead = "00" then
						s_ALU_pre_B <= s_PIPE_MEM_WB.a_ALURet;
					else
						s_ALU_pre_B <= io_DATA; --s_PIPE_MEM_WB.a_MemData;
					end if;
				when others =>
			end case;
	end process;
	
		-- MUX
	-- Forwarding for ALU B
	process (s_ALUSrcB,
				s_ALU_B,
				s_PIPE_ID_EXE,
				s_ALU_pre_B)
	begin
		if s_PIPE_ID_EXE.a_RegDst = '0' then
			s_ALU_B <= s_PIPE_ID_EXE.a_immediate;
		else
			s_ALU_B <= s_ALU_pre_B;
		end if;
	end process;

	id0: entity work.cpu_id port map(IN_CLK 	=> IN_CLK,
								IN_RESET 	=> IN_RESET,
								IN_REG => s_REG,
								IN_PIPE_IF_ID => s_PIPE_IF_ID,
								OUT_PIPE_ID_EXE => s_PIPE_ID_EXE,
								OUT_bubble => s_bubble);
	
	-- ALU :-P
	alu0: entity work.cpu_alu port map(	IN_CLK 	=> IN_CLK,
								IN_OP		=> s_PIPE_ID_EXE.a_ALUOP,
								IN_SA		=> s_PIPE_ID_EXE.a_shamt,
								IN_A 		=> s_ALU_A,
								IN_B 		=> s_ALU_B,
								OUT_R 	=> s_ALURet);
	
end architecture;




