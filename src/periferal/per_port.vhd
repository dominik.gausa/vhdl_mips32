-- REG 0: Direction (Input = 0; Output = 1)
-- REG 1: READ / WRITE


LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.work_pkg.all;

ENTITY per_port IS
	generic ( start_address: integer := 0
			);
	port (IN_CLK		: in STD_LOGIC;
			IN_RESET		: in STD_LOGIC;
			IN_ADDRESS	: in std_logic_vector(31 downto 0);
			IN_ADDRESS_R	: in STD_LOGIC;
			IN_ADDRESS_W	: in std_logic_vector(1 downto 0);
			io_DATA	: inout STD_LOGIC_VECTOR (31 downto 0);
			io_PINS : inout STD_LOGIC_VECTOR (15 downto 0)
			);
END per_port;

ARCHITECTURE arch of per_port IS
	constant port_reg_offset_in : integer := start_address/4;
	constant port_reg_offset_out : integer := start_address/4+1;
	constant port_reg_offset_dir : integer := start_address/4+2;

	signal s_IN : STD_LOGIC_VECTOR (io_PINS'left downto 0) := (others => '0');
	signal s_registers : t_32BitArray(port_reg_offset_in to port_reg_offset_dir) := (
			X"00000000",
			X"00000000",
			X"00000000");
BEGIN
	
	process (IN_CLK,
				IN_RESET)
	begin
		if IN_RESET = '1' then
			io_PINS <= (others => 'Z');
		else
			if rising_edge(IN_CLK) then
				s_registers(port_reg_offset_in)(io_PINS'left downto 0) <= io_PINS;
				
				for i in 0 to io_PINS'left loop
					if s_registers(port_reg_offset_dir)(i) = '1' then
						io_PINS(i) <= s_registers(port_reg_offset_out)(i);
					else
						io_PINS(i) <= 'Z';
					end if;
				end loop;
			end if;
		end if;
	end process;
	
	
	process (IN_CLK,
				IN_RESET)
	begin
		if rising_edge(IN_CLK) then
			io_DATA <= (others => 'Z');
			case to_integer(unsigned(IN_ADDRESS(31 downto 2))) is	
				when port_reg_offset_dir =>
					if IN_ADDRESS_W /= "00" then
						s_registers(port_reg_offset_dir) <= io_DATA;
					end if;
					if IN_ADDRESS_R = '1' then
						io_DATA <= s_registers(port_reg_offset_dir);
					end if;
				when port_reg_offset_out =>
					if IN_ADDRESS_W /= "00" then
						s_registers(port_reg_offset_out) <= io_DATA;
					end if;
					if IN_ADDRESS_R = '1' then
						io_DATA <= s_registers(port_reg_offset_out);
					end if;
				when port_reg_offset_in =>
					if IN_ADDRESS_R = '1' then
						io_DATA <= s_registers(port_reg_offset_in);
					end if;
				when others =>
			end case;
		end if;
	end process;
	
END arch;