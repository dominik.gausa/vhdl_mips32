-- REG 0: EN 31, Pulse width 15..0
-- Use with CLK_50M ONLY !!
-- LOWER:  C350
-- MID:   124F8
-- UPPER: 186A0


LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.work_pkg.all;

ENTITY per_pwm IS
	port (IN_CLK		: in STD_LOGIC;
			IN_RESET		: in STD_LOGIC;
			IN_REGISTERS	: in t_32BitArray(0 to 0);
			OUT_REGISTERS	: out t_32BitArray(0 to 0);
			OUT_State		: out STD_LOGIC
			);
END per_pwm;

ARCHITECTURE arch of per_pwm IS
	signal s_count : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
	signal s_pulse : STD_LOGIC_VECTOR (15 downto 0) := std_logic_vector(to_unsigned(512,16)+990);
BEGIN
	
	
	process (IN_CLK,
				IN_RESET,
				IN_REGISTERS)
	begin
		if IN_RESET = '1' then
			s_count <= (others => '0');
		else
			if rising_edge(IN_CLK) then
				OUT_State <= '0';
				if IN_REGISTERS(0)(31) = '1' then
					if unsigned(s_count) < unsigned(s_pulse) then
						OUT_State <= '1';
					end if;
					
					if unsigned(s_count) >= 20000 then
						s_count <= (others => '0');
					else
						s_count <= std_logic_vector(unsigned(s_count) + 1);
					end if;
				end if;
			else
			end if;
		end if;
	end process;

	process (IN_CLK,
				IN_RESET,
				IN_REGISTERS)
	begin
		if IN_RESET = '1' then
		else
			if rising_edge(IN_CLK) then
				s_pulse <= std_logic_vector(unsigned(IN_REGISTERS(0)(15 downto 0))+990);
				if 1023 < unsigned(IN_REGISTERS(0)(15 downto 0)) then
					s_pulse <= std_logic_vector(to_unsigned(1023,16)+990);
				end if;
				
			end if;
		end if;
	end process;
	
	OUT_REGISTERS(0) <= IN_REGISTERS(0);
END arch;