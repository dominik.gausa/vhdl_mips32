LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.work_pkg.all;

ENTITY per_counter IS
	port (IN_CLK		: in STD_LOGIC;
			IN_RESET		: in STD_LOGIC;
			IN_CONF		: in STD_LOGIC_VECTOR (31 downto 0);
			OUT_VAL		: out STD_LOGIC_VECTOR (31 downto 0)
			);
END per_counter;

ARCHITECTURE arch of per_counter IS
	signal s_count : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
	
BEGIN
	
	process (IN_CLK,
				IN_RESET)
	begin
		if IN_RESET = '1' then
			s_count <= (others => '0');
		else
			if rising_edge(IN_CLK) then
				if IN_CONF(31) = '1' then
					OUT_VAL(31) <= '0';
					if s_count >= "00000000" & IN_CONF(23 downto 0) then
						s_count <= (others => '0');
						OUT_VAL(31) <= '1';
					else
						s_count <= s_count + 1;
					end if;
				end if;
			end if;
		end if;
	end process;
	
	OUT_VAL(30 downto 24) <= (others => '0');
	OUT_VAL(23 downto 0) <= s_count(23 downto 0);
END arch;