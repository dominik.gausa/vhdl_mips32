-- Copyright (C) 1991-2012 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 32-bit"
-- VERSION		"Version 12.1 Build 243 01/31/2013 Service Pack 1 SJ Web Edition"
-- CREATED		"Wed May 15 12:05:58 2013"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

LIBRARY work;
USE work.basic_pkg.all;
use work.work_pkg.all;


ENTITY soc IS 
	PORT
	(
		CLK :  IN  STD_LOGIC;
		IN_RESET :  IN  STD_LOGIC;
		
		IN_Prog_CLK : IN STD_LOGIC;
		IN_PROG_DATA : IN STD_LOGIC;
		io_PORTA		: inout STD_LOGIC_VECTOR (15 downto 0);
		io_PORTB		: inout STD_LOGIC_VECTOR (15 downto 0)
	);
END soc;

ARCHITECTURE bdf_type OF soc IS 

	-- S I G N A L S
	SIGNAL	CLK_200M :  STD_LOGIC := '0';
	SIGNAL	CLK_100M :  STD_LOGIC := '0';
	SIGNAL	CLK_32M :  STD_LOGIC := '0';
	SIGNAL	CLK_1M :  STD_LOGIC := '0';
	SIGNAL	CLK_SYS :  STD_LOGIC := '0';
	SIGNAL	RESET_SYS :  STD_LOGIC := '1';
	SIGNAL	RESET_SYS_n :  STD_LOGIC := '0';
	
	
	signal s_counter : std_LOGIC_VECTOR(24 downto 0) := (others => '0');
	signal s_PC : std_LOGIC_VECTOR(31 downto 0) := (others => '0');
	
	signal s_data_address : std_logic_vector(31 downto 0);
	signal s_data_r : std_logic := '0';
	signal s_data_w : std_logic_vector(1 downto 0);
	signal s_data : std_logic_vector(31 downto 0);
BEGIN 

	
	clk_div0 : entity work.clk_div
	port map (IN_CLK => CLK_200M,
				 IN_RESET => RESET_SYS,
				 OUT_CLK => CLK_SYS
				 );



	b2v_inst2 : entity work.pll
	PORT MAP(inclk0 => CLK,
			 c0 => CLK_200M,
			 c1 => CLK_100M,
			 c2 => CLK_32M,
			 c3 => CLK_1M,
			 areset => IN_RESET,
			 locked => RESET_SYS_n);

	RESET_SYS <= not RESET_SYS_n;

	
	cpu0 : entity work.cpu
	PORT MAP(IN_CLK => CLK_SYS,
				IN_RESET => RESET_SYS,
				IN_Prog_CLK => IN_Prog_CLK,
				IN_PROG_DATA => IN_PROG_DATA,
				OUT_PC => s_PC,
				OUT_ADDRESS 	=> s_data_address,
				OUT_ADDRESS_R	=> s_data_r,
				OUT_ADDRESS_W	=> s_data_w,
				io_DATA			=> s_data);


	port0: entity work.per_port
			generic map( start_address => REGISTER_START
			)
	port map(IN_CLK		=> CLK_SYS,
			IN_RESET		=> IN_RESET,
			IN_ADDRESS	=> s_data_address,
			IN_ADDRESS_R	=> s_data_r,
			IN_ADDRESS_W	=> s_data_w,
			io_DATA	=> s_data,
			io_PINS => io_PORTA
			);
	
	ram0: entity work.ram
	port map(IN_CLK		=> CLK_SYS,
			IN_RESET		=> IN_RESET,
			IN_ADDRESS	=> s_data_address,
			IN_ADDRESS_R	=> s_data_r,
			IN_ADDRESS_W	=> s_data_w,
			io_DATA	=> s_data
			);

END bdf_type;