LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

package basic_pkg is
	type t_8BitArray 			is array(natural range <>) of std_logic_vector(7 downto 0);
	type t_32BitArray 		is array(natural range <>) of std_logic_vector(31 downto 0);

	-- obsolete
	type typ_ROM 			is array(natural range <>) of std_logic_vector(7 downto 0);

	constant ROM_START	: natural := 	to_integer(unsigned(to_stdlogicvector(X"00000000")));
	constant ROM_SIZE		: natural := 	1024/4;
	constant ROM_END		: natural := 	ROM_START + ROM_SIZE - 1;

	constant RAM_START	: natural := 	to_integer(unsigned(to_stdlogicvector(X"40000000")));
	constant RAM_SIZE		: natural := 	1024/4;
	constant RAM_END		: natural := 	RAM_START + RAM_SIZE - 1;
	
	constant REGISTER_START	: natural := 	to_integer(unsigned(to_stdlogicvector(X"10000000")));
	
	type typ_REGISTER 	is array(natural range <>) of std_logic_vector(31 downto 0);
	constant REGISTER_N	: natural := 	32;
	
end basic_pkg;
