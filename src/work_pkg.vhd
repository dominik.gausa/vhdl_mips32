LIBRARY ieee;
USE ieee.std_logic_1164.all;
-- USE ieee.std_logic_unsigned.all;

package work_pkg is

        constant CLK_DIV_1      : integer := 0;
        constant CLK_DIV_2      : integer := 1;
        constant CLK_DIV_4      : integer := 2;
        constant CLK_DIV_8      : integer := 3;
        constant CLK_DIV_16     : integer := 4;
        constant CLK_DIV_32     : integer := 5;
        constant CLK_DIV_64     : integer := 6;
        constant CLK_DIV_128    : integer := 7;
        constant CLK_DIV_256    : integer := 8;
        constant CLK_DIV_512    : integer := 9;                                                                
        constant CLK_DIV_1024: integer := 10;                                                                  
        constant CLK_DIV_SYS    : integer := CLK_DIV_4;

end work_pkg;
