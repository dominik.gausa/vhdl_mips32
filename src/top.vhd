LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

LIBRARY work;
use work.work_pkg.all;


ENTITY top IS
	port (CLK: in STD_LOGIC;
			in_KEY: in STD_LOGIC_VECTOR(1 downto 0);
			io_GPIO_0: inout STD_LOGIC_VECTOR(0 to 33);
			in_GPIO_0_IN: in STD_LOGIC_VECTOR(0 to 1);
			io_GPIO_1: inout STD_LOGIC_VECTOR(0 to 33);
			in_GPIO_1_IN: in STD_LOGIC_VECTOR(0 to 1);
			in_SWITCH: in STD_LOGIC_VECTOR(0 to 3);
			out_LED: out STD_LOGIC_VECTOR(0 to 7);
			out_ADC_CS_N: out STD_LOGIC;
			out_ADC_SCLK: out STD_LOGIC;
			out_ADC_SADDR: out STD_LOGIC;
			in_ADC_SDAT: in STD_LOGIC
			);
END top;



ARCHITECTURE arch of top IS
	signal s_reset : std_logic := '1';

BEGIN
	s_reset <= not in_KEY(0);
	
	soc0:  entity work.soc 
				port map(CLK => CLK,
							IN_RESET => s_reset,
							IN_Prog_CLK => in_GPIO_0_IN(0),
							IN_PROG_DATA => in_GPIO_0_IN(1),
							io_PORTA => io_GPIO_0(0 to 15),
							io_PORTB => io_GPIO_1(0 to 15)
							);
	
end arch;



