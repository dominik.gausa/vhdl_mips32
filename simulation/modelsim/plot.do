vsim top_tb
view wave -undock -title Signals

del wave *


#add wave -radix hex
proc plot { inst } {
  echo plotting $inst "\n"
  set entity_name "top"
  set expand " "
  set expand_sigs " -expand "
  set hirac [string trim [string map {"/" " "} $inst] ]
  set group [join [split $hirac " "] " $expand -group " ]

#    eval add wave -group $group -divider -height 32 CLOCK
    foreach sig [ find signals /$inst/CLK* ] { 
      eval add wave -group $group $expand_sigs -group CLOCK -color #F5BCA9 $sig
      }
    foreach sig [ find signals /$inst/RESET* ] {
      eval add wave -group $group $expand_sigs -group CLOCK -color #FE642E $sig
      }
    
#    eval add wave -group $group -divider -height 32 INTERFACE
    foreach sig [ find signals /$inst/in_* ] {
      eval add wave -group $group $expand_sigs -group INTERFACE -color #FF8000 $sig
      }
    foreach sig [ find signals /$inst/out_* ] {
      eval add wave -group $group $expand_sigs -group INTERFACE -color #00FF00 $sig
      }
    foreach sig [ find signals /$inst/io_* ] {
      eval add wave -group $group $expand_sigs -group INTERFACE -color #2E64FE $sig
      }

#    eval add wave -group $group -noupdate -divider -height 32 SIGNALS
    foreach sig [ find signals /$inst/s_* ] { 
      eval add wave -group $group $expand_sigs -group SIGNALS -color #00BFFF $sig
      }
}

foreach ent [lsort [find instances -recursive /top_tb/* ] ] {
  echo $ent "\n"
  plot [lindex $ent 0]
}

  set BoardName "DE0-Nano"
  add wave -noupdate -divider -height 32 EXTERN
  add wave -color #F3F781 -group $BoardName -expand -group GPIO /top/*GPIO_*
  add wave -color #40FF00 -group $BoardName -expand -group OUTPUTS -expand /top/out_LED*
  add wave -color #FA58F4 -group $BoardName -expand -group INPUTS -expand /top/in_KEY*
  add wave -color #FA58F4 -group $BoardName -expand -group INPUTS -expand /top/in_SWITCH*
  add wave -color #00BFFF -group $BoardName -expand -group SIGNALS -expand /top_tb/s_*
  
#run 100000000
run 300us
